<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_amap_distribution' => 'Ajouter cette distribution',

	// C
	'champ_date_label' => 'Date',
	'champ_id_amap_periode_label' => 'Période',
	'champ_texte_label' => 'Texte',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_amap_distribution' => 'Confirmez-vous la suppression de cette distribution ?',

	// I
	'icone_creer_amap_distribution' => 'Créer une distribution',
	'icone_modifier_amap_distribution' => 'Modifier cette distribution',
	'info_1_amap_distribution' => 'Une distribution',
	'info_amap_distributions_auteur' => 'Les distributions de cet auteur',
	'info_aucun_amap_distribution' => 'Aucune distribution',
	'info_nb_amap_distributions' => '@nb@ distributions',

	// R
	'retirer_lien_amap_distribution' => 'Retirer cette distribution',
	'retirer_tous_liens_amap_distributions' => 'Retirer toutes les distributions',

	// S
	'supprimer_amap_distribution' => 'Supprimer cette distribution',

	// T
	'texte_ajouter_amap_distribution' => 'Ajouter une distribution',
	'texte_changer_statut_amap_distribution' => 'Cette distribution est :',
	'texte_creer_associer_amap_distribution' => 'Créer et associer une distribution',
	'texte_definir_comme_traduction_amap_distribution' => 'Cette distribution est une traduction de la distribution numéro :',
	'titre_amap_distribution' => 'Distribution',
	'titre_amap_distributions' => 'Distributions',
	'titre_amap_distributions_rubrique' => 'Distributions de la rubrique',
	'titre_langue_amap_distribution' => 'Langue de cette distribution',
	'titre_logo_amap_distribution' => 'Logo de cette distribution',
);