<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_amap_periode' => 'Ajouter cette période de commande',

	// C
	'champ_date_limite_label' => 'Date limite des commandes',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_amap_periode' => 'Confirmez-vous la suppression de cette période de commande ?',

	// I
	'icone_creer_amap_periode' => 'Créer une période de commande',
	'icone_modifier_amap_periode' => 'Modifier cette période de commande',
	'info_1_amap_periode' => 'Une période de commande',
	'info_amap_periodes_auteur' => 'Les périodes de commande de cet auteur',
	'info_aucun_amap_periode' => 'Aucune période de commande',
	'info_nb_amap_periodes' => '@nb@ périodes de commande',

	// R
	'retirer_lien_amap_periode' => 'Retirer cette période de commande',
	'retirer_tous_liens_amap_periodes' => 'Retirer toutes les périodes de commande',

	// S
	'statut_archive' => 'archivée',
	'statut_poubelle' => 'à la poubelle',
	'statut_prepa' => 'en préparation',
	'statut_prod' => 'ouverte aux producteurs',
	'statut_publie' => 'ouverte à la commande',
	'supprimer_amap_periode' => 'Supprimer cette période de commande',

	// T
	'texte_ajouter_amap_periode' => 'Ajouter une période de commande',
	'texte_changer_statut_amap_periode' => 'Cette période de commande est :',
	'texte_creer_associer_amap_periode' => 'Créer et associer une période de commande',
	'texte_definir_comme_traduction_amap_periode' => 'Cette période de commande est une traduction de la période de commande numéro :',
	'titre_amap_periode' => 'Période de commande',
	'titre_amap_periodes' => 'Périodes de commande',
	'titre_amap_periodes_rubrique' => 'Périodes de commande de la rubrique',
	'titre_langue_amap_periode' => 'Langue de cette période de commande',
	'titre_logo_amap_periode' => 'Logo de cette période de commande',
);
